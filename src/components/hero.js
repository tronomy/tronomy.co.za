import React from 'react';
const image = "/logo.png";
const Hero = () => (
    <section id="Header" className="flex flex-column flex-auto w-100  bb b--black-05 bg-gradient-blue">

        <div className="flex flex-row mw8 center w-100 pv5">


            <div            
            id="HeroImage" 
            className="f5 fw6 black contain bg-center h5 w-100"
            style={{backgroundImage: `url(`+ image +`)`}}
            ></div>
    
     
        </div>
    </section>
)
export default Hero