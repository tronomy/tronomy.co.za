import React from 'react';
// import { Link } from 'react-router-dom';

const items = [
    {title: "Home", link: "#Home"},
    {title: "Services", link: "#Services"},
    {title: "Contact Us", link: "#Contact"},
    {title: "Clients", link: "#Clients"}
]
const Header = () => (
    <section id="Header" className="flex flex-column flex-auto w-100 bb b--black-05">

        <div className="flex flex-row mw8 center w-100">

        <div className="flex flex-row w-30">
            <div id="Logo" className="f5 fw6 black pv3 "><img src="/logo.png" /></div>
        </div>
        <div className="flex flex-row w-70 items-center justify-end">
            {
                items.map((item,index)=>(
                    <a title={item.title} href={item.link} className="flex ph4 pv3 f6 fw5 black">{item.title}</a>
                ))
            }
        </div>
        </div>
    </section>
)
export default Header