import React from 'react';
import Carousel, { Dots } from '@brainhubeu/react-carousel';

const images = [
    "1.jpg",
    "2.png",
    "3.jpg",
    "4.png",
  
    "6.png",
    "7.png",

    "9.jpg",
    "10.png",
    "11.png",
    "12.png",
    "13.png",
    "14.jpg",
    "15.jpg",
    "16.jpg",
    "17.png",
    "18.jpeg",
]
const Banner = () => (
    <section id="Clients" className="flex flex-column flex-auto w-100  bb b--black-05 bg-white">
        <div className="flex flex-column mw8 center w-100 pv4">
            <div
                className="f2 fw6 black contain bg-center  w-100"
            >
                <h3>Who we've worked with.</h3>
            </div>
            <div className="flex flex-column pv5">
            <Carousel
                slidesPerPage={4}
                arrows
                infinite
            >
                {
                    images.map((item, index) => (
                        <div className="ph3"><img className="mh3" key={index} src={"/carousel/" + item} /></div>
                    ))
                }
            </Carousel>
            </div>
        </div>
    </section>
)
export default Banner