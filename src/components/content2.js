import React from 'react';
const image = "/logo.png";
const items = [
    {
        title: "Design",
        content: "The highly creative Tronomy team will deliver the highest quality design solution for your idea. We consult, do in-depth research, and design fun, innovative, easy to use prototypes."
    },
    {
        title: "Develop",
        content: "By understanding the requirements and using the latest technology, we ensure each and every client’s idea is brought to life on time and on budget."
    },
    {
        title: "QA",
        content: "Websites and Apps are the connection between your company and the world.  Our trained testers will ensure that you have a bug free version of your App or Website to ensure you turn browsers into customers."
    },
    {
        title: "Launch",
        content: "Once the idea has been brought to life, we will guide you to launch your App to the world, by supporting and guiding you through the Web and Apple store requirements and initial launch period."
    },
];
const Content2 = () => (
    <section id="Services" className="flex flex-column flex-auto w-100  bb b--black-05 bg-black-10">

        <div className="flex flex-column mw8 center w-100 pv4">


            <div
                className="f2 fw6 black contain bg-center  w-100"
            >
                <h3>How can we assist you with your vision?</h3>
            </div>

            <div className="flex flex-column pv5 mw7 center">
                <img src="/home-artwor-trans.png" className=""/>
            </div>

            <div className="flex flex-column flex-row-ns col4">
                {
                    items.map((item,index) => (
                        <div key={index} className="flex flex-column ph2 tl">
                            <h3 className="f4 fw6 black-80">{item.title}</h3>
                            <p className="f5 fw4 black-60">{item.content}</p>
                        </div>
                    ))
                }
            </div>

        </div>
    </section>
)
export default Content2