import React from 'react';
const image = "/logo.png";
const Banner = () => (
    <section id="Header" className="flex flex-column flex-auto w-100  bb b--black-05 bg-black-80">

        <div className="flex flex-row mw8 center w-100 pv4">


            <div                        
            className="f2 fw6 white contain bg-center  w-100"
            >
                <h3>We Make Apps Happen!</h3>
            </div>
    
     
        </div>
    </section>
)
export default Banner