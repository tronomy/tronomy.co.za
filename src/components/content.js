import React from 'react';
const image = "/logo.png";
const Content = () => (
    <section id="Content" className="flex flex-column flex-auto w-100">

        <div className="row-1 flex flex-column mw8 center w-100 pv5 bb b--black-05">

            <div
            className="flex flex-row f5 fw6 black contain bg-center  w-100"            
            >
                <div
                className="w-100 w-50-ns flex flex-column f5 fw6 black w-100"            
                >
                    <div className="flex flex-column f3 fw5 tl">
                        Why Choose Tronomy?
                    </div>
                    <div className="flex flex-column f6 fw4 tl">
                       <p className="f5 fw4 black-60">
                       We are a group of highly enthusiastic individuals with a fresh outlook on the development of Mobile App’s, Systems and Website Design.  We use the latest technology and tools in delivering world-class mobile App development and designing services across all platforms.
                       </p>
                       <p className="f5 fw4 black-60">
                       We demonstrate a solid understanding of core technology.
                       </p>
                       <p className="f5 fw4 black-60">
                       We are able to rapidly investigate and successfully develop prototypes across old and new emerging technology platforms.
                       </p>
                    </div>

                </div>

                <div
                className="flex w-100 w-50-ns f5 fw6 black contain bg-center mw6 w-100 items-start justify-end"
                >
                    <img src="/2.jpg" className="br2 bs-a content-image-1" />
                </div>

            </div>           
    
     
        </div>


        <div className="row-2 flex flex-column mw8 center w-100 pv5 bb b--black-05">

            <div
            className="flex flex-row f5 fw6 black contain bg-center  w-100"            
            >
                <div
                className="flex w-100 w-50-ns f5 fw6 black contain bg-center mw6 w-100 items-start justify-start"
                >
                    <img src="/Software-Development-2.jpg" className="br2 bs-a content-image-1" />
                </div>

                <div
                className="w-100 w-50-ns flex flex-column f5 fw6 black w-100"            
                >
                   
                    <div className="flex flex-column f6 fw4 tl">
                       <p className="f5 fw4 black-60">
                       We have successfully delivered services to industry-leading companies.
                       </p>
                       <p className="f5 fw4 black-60">
                       We revolutionize our clients’ establishments by keeping them ahead of constant evolving technology.
                       </p>
                       <p className="f5 fw4 black-60">
                       We focus on our clients’ goals by presenting progressive ideas, offer support, practice techniques to utilize advanced infrastructure, and meet exact requirements on time and within budget.
                       </p>
                    </div>

                </div>

                

            </div>           
    
     
        </div>


    </section>
)
export default Content