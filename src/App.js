import React from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './components/header.js';
import Hero from './components/hero.js';
import Content from './components/content.js';
import Banner from './components/banner.js';
import Content2 from './components/content2.js';
import Carousel from './components/carousel.js';

function App() {
  return (
    <div className="App">
        <Header />
        <Hero />
        <Content />
        <Banner /> 
        <Content2 />
        <Carousel />
    </div>
  );
}

export default App;
